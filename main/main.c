#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "driver/gpio.h"

#define LED_1 2
#define LED_2 4
#define LED_3 16
#define LED_4 17
#define BOTAO 15

void app_main()
{
  // Configuração dos pinos dos LEDs 
  gpio_pad_select_gpio(LED_1);   
  gpio_pad_select_gpio(LED_2);
  gpio_pad_select_gpio(LED_3);
  gpio_pad_select_gpio(LED_4);
  // Configura os pinos dos LEDs como Output
  gpio_set_direction(LED_1, GPIO_MODE_OUTPUT);  
  gpio_set_direction(LED_2, GPIO_MODE_OUTPUT);
  gpio_set_direction(LED_3, GPIO_MODE_OUTPUT);
  gpio_set_direction(LED_4, GPIO_MODE_OUTPUT);

  // Configuração do pino do Botão
  gpio_pad_select_gpio(BOTAO);
  // Configura o pino do Botão como Entrada
  gpio_set_direction(BOTAO, GPIO_MODE_INPUT);
  // Configura o resistor de Pulldown para o botão (por padrão a entrada estará em Zero)
  gpio_pulldown_en(BOTAO);
  // Desabilita o resistor de Pull-up por segurança.
  gpio_pullup_dis(BOTAO);

  // Teste os LEDs Externos
  for(int i = 0; i <= 8; i++)
  {
    gpio_set_level(LED_2, (i&1) >> 0);
    gpio_set_level(LED_3, (i&2) >> 1);
    gpio_set_level(LED_4, (i&4) >> 2);

    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }

  // Testa o Botão utilizando polling
  while (true)
  {
    int estado_botao = gpio_get_level(BOTAO);
    gpio_set_level(LED_1, estado_botao);
    vTaskDelay(100 / portTICK_PERIOD_MS);    
  }  
}